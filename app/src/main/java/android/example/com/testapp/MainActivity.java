package android.example.com.testapp;

import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.roger.catloadinglibrary.CatLoadingView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    private String TAG = MainActivity.class.getSimpleName();

    //  UI For Setting Images
    private RecyclerView gridView;

    //  Getting UrlList of Photos
    List<GridViewAdapter.Item> items = new ArrayList<>();

    //  Creating Request String...
    String Main = "https://api.flickr.com/services/rest/";
    String Method = "?method=flickr.galleries.getPhotos";
    String API_Key = "&api_key=4efc7fd5c901b80f202b672d71f198e2";
    String Gallery_ID = "&gallery_id=66911286-72157692049980335";
    String Format = "&format=json";
    String jsonCallBack = "&nojsoncallback=1";

    //  JSON URL
    String URL = GetCompleteURL(Main, Method, API_Key, Gallery_ID, Format, jsonCallBack);

    // Will be used in future
    ArrayList<String> URLs = new ArrayList<>();
    ArrayList<String> listURLS = new ArrayList<>();
    ArrayList<String> Titles = new ArrayList<>();
    ArrayList<String> Details = new ArrayList<>();

    //For Custom ProgressBar
    CatLoadingView mView;

    private int currDescriptionPosition = -1;

    //  For Checking Connectivity
    public NetworkChangeReceiver receiver;

    public boolean checkInternet() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new NetworkChangeReceiver(this);
        registerReceiver(receiver, filter);
        return receiver.is_connected();
    }

    private void LoadImages(MainActivity mainActivity) {
        if (checkInternet()) {
            StringRequest stringRequest = new StringRequest(URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("Respone", "onResponse: " + response);

                    //  Parsing JSON
                    ParseJSON(response);
                }
            }, error -> {
                mView.dismiss();
                Log.d(TAG, "onErrorResponse: Error Occured...");
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
            mView.show(getSupportFragmentManager(), "Loading...");
        } else {
            Toast.makeText(this, "Turn Internet on...", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gridView = findViewById(R.id.gridview);

        if (checkInternet()) {
            mView = new CatLoadingView();
            LoadImages(this);
        } else {
            mView.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    private String GetCompleteURL(String Main, String Method, String API_Key, String Gallery_ID, String Format, String jsonCallBack) {
        return Main + Method + API_Key + Gallery_ID + Format + jsonCallBack;
    }

    void ParseJSON(String URL) {
        try {
            JSONObject root = new JSONObject(URL);
            JSONObject photos = root.getJSONObject("photos");
            JSONArray photo = photos.getJSONArray("photo");

            for (int i = 0; i < photo.length(); i++) {
                JSONObject photosJSONObject = photo.getJSONObject(i);
                String FarmID = photosJSONObject.getString("farm");
                String ServerID = photosJSONObject.getString("server");
                String ID = photosJSONObject.getString("id");
                String SecretID = photosJSONObject.getString("secret");
                String ImageTitle = photosJSONObject.getString("title");

                listURLS.add(i, CreatePhotoURL(FarmID, ServerID, ID, SecretID));
                Titles.add(i, ImageTitle);

                URLs.add(i, "https://api.flickr.com/services/rest/?method=flickr.photos.getInfo&api_key=" + API_Key + "&photo_id=" + ID + "&format=json&nojsoncallback=1");
            }

            AddtoList(URLs);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void AddtoList(ArrayList<String> CreateURL) {
        for (int i = 0; i < CreateURL.size(); i++) {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            StringRequest stringRequest = new StringRequest(CreateURL.get(i), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    JSONObject root;
                    try {
                        root = new JSONObject(response);
                        JSONObject photo = root.getJSONObject("photo");

                        String username = photo.getJSONObject("owner").getString("username");
                        String DateTaken = photo.getJSONObject("dates").getString("taken");
                        String Views = photo.getString("views");

                        Details.add("Date Taken : " + DateTaken + "\n" + "Views : " + Views + "\n" + "User Name : " + username + "\n");
                        if(Details.size()==CreateURL.size()){
                            UpdateUI();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, "onErrorResponse: " + error.toString());
                }
            });
            requestQueue.add(stringRequest);
        }
    }

    private void UpdateUI() {

        for (String urls : listURLS) {
            Log.d("urls", urls);
        }

        for (String title : Titles) {
            Log.d("tts", title);
        }

        for (String dt : Details) {
            Log.d("dts", dt);
        }

        //  Setting up GridView
        items.clear();

        for (int i = 0; i < listURLS.size(); i++) {
            GridViewAdapter.Item item = new GridViewAdapter.Item(listURLS.get(i), Titles.get(i),Details.get(i));
            items.add(item);
        }

        final GridLayoutManager glm = new GridLayoutManager(MainActivity.this, 2);
        final GridViewAdapter gridViewAdapter = new GridViewAdapter(MainActivity.this, items);

        gridViewAdapter.setItemClickListener((clickedPosition, clickedView) -> {
            if (currDescriptionPosition != -1) {
                items.remove(currDescriptionPosition);
                if (currDescriptionPosition <= clickedPosition) {
                    clickedPosition--;
                }
            }
            currDescriptionPosition = clickedPosition + (clickedPosition % 2 == 0 ? 2 : 1);
            items.add(currDescriptionPosition, new GridViewAdapter.Item("description", Titles.get(clickedPosition), Details.get(clickedPosition)));
            glm.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    if (position == currDescriptionPosition) {
                        return 2;
                    } else {
                        return 1;
                    }
                }
            });
            gridViewAdapter.notifyDataSetChanged();
        });
        gridView.setLayoutManager(glm);
        gridView.setAdapter(gridViewAdapter);
        mView.dismiss();
    }

    String CreatePhotoURL(String FarmID, String ServerID, String ID, String SecretID) {
        return "https://farm"
                + FarmID
                + ".staticflickr.com/"
                + ServerID
                + "/"
                + ID
                + "_"
                + SecretID
                + ".jpg";
    }
}