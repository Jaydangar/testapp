package android.example.com.testapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import java.util.List;

public class GridViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<Item> items;
    private ItemClickListener clickListener;

    //  For Tap to Hide/Show functionality
    boolean GONE = false;

    public GridViewAdapter(Context context, List<Item> items) {
        this.context = context;
        this.items = items;
    }

    public void setItemClickListener(ItemClickListener listener) {
        this.clickListener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.itemview, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        Item item = items.get(position);
        if (item.url.equals("description")) {
            viewHolder.imageView.setVisibility(View.GONE);
            viewHolder.textView.setText(item.Details);
            if(!GONE){
                viewHolder.textView.setVisibility(View.VISIBLE);
                GONE = true;
            }
            else{
                viewHolder.textView.setVisibility(View.GONE);
                GONE=false;
            }

        } else {
            Glide.with(context).load(items.get(position).url).into(viewHolder.imageView);
            viewHolder.imageView.setVisibility(View.VISIBLE);
            viewHolder.textView.setText(items.get(position).title);
        }
        viewHolder.rootView.setOnClickListener(view -> {
            clickListener.onItemClick(position, view);
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;
        View rootView;

        public ViewHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageView);
            textView = itemView.findViewById(R.id.text);
            rootView = itemView.findViewById(R.id.rootView);
        }
    }

    public interface ItemClickListener {
        void onItemClick(int position, View view);
    }

    static class Item {
        String url;
        String title;
        String Details;

        public Item(String url, String title,String Details) {
            this.url = url;
            this.title = title;
            this.Details = Details;
        }
    }
}